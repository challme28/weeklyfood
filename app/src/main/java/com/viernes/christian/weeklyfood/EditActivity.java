package com.viernes.christian.weeklyfood;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Christian on 2/12/2016.
 */
public class EditActivity extends AppCompatActivity{
    @Bind(R.id.recycler_edit)
    RecyclerView rv1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        ArrayList<String> myDataset =new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.comidas)));
        Log.d("EditActivity", myDataset.get(2));

        LinearLayoutManager llm= new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv1.setLayoutManager(llm);
        rv1.setHasFixedSize(true);
        RecyclerView.Adapter mAdapter = new MyAdapter(myDataset);
        rv1.setAdapter(mAdapter);




    }
}

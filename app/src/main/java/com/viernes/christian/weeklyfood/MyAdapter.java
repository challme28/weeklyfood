package com.viernes.christian.weeklyfood;

import java.util.ArrayList;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Christian on 2/12/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<String> mDataset;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_edit, parent, false);
        ViewHolder vh = new ViewHolder(v);
        Log.d("MyAdapter", "onCreateViewHolder");
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final String name= mDataset.get(position);
        holder.txt.setText(mDataset.get(position));
        Log.d("MyAdapter", position+"");

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txt;
        public ViewHolder(View itemView) {
            super(itemView);
            txt= (TextView)itemView.findViewById(R.id.textView);
            Log.d("MyAdapter", "ViewHolder");

        }
    }

    public void add(int position, String item){
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item){
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public MyAdapter(ArrayList<String> myDataset) {
        mDataset = myDataset;
        Log.d("MyAdapter", "MyAdapter");
    }
}
